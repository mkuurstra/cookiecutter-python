import sys
import requests


def download_file(url: str, output_filename: str) -> None:
    """
    Downloads content from a given URL and saves it to a specified file.

    Args:
        url: The URL to download from
        output_filename: The name of the file to save the downloaded content to

    Raises:
        Exception: If there's an error during download or file writing
    """
    try:
        import requests

        # Send GET request to URL
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception for bad status codes

        # Write the content to the specified file
        with open(output_filename, "wb") as file:
            file.write(response.content)

    except Exception as e:
        print(f"Error downloading file: {str(e)}", file=sys.stderr)
        raise


if __name__ == "__main__":
    download_file("https://gitlab.com/mkuurstra-works/cookiecutter/centralized-files/-/raw/main/.editorconfig", ".editorconfig")
