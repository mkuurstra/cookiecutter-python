import os
import setuptools


def read(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="{{cookiecutter.repo_name}}",
    version=read("VERSION"),
    long_description=read("README.md"),
    long_description_content_type="text/markdown",
    packages=["{{cookiecutter.repo_name}}"],
    install_requires=[],
    entry_points={
        "console_scripts": [
            "{{cookiecutter.repo_name}} = {{cookiecutter.repo_name}}.{{cookiecutter.repo_name}}:cli"
        ]
    },
)
