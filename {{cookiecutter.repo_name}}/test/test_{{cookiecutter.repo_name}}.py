from {{cookiecutter.repo_name}} import {{cookiecutter.repo_name}}


def test_return_hello_world() -> None:
    assert {{cookiecutter.repo_name}}.return_hello_world() == "Hello World!"
