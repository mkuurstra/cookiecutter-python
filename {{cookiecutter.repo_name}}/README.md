# {{cookiecutter.project_name}}

## Setup

```sh
# Install dependencies
pipenv install --dev

# Setup pre-commit and pre-push hooks
pipenv run pre-commit install -t pre-commit
pipenv run pre-commit install -t pre-push
```

## Credits

This package was created with Cookiecutter and the [mkuurstra-works/cookiecutter/cookiecutter-python](https://gitlab.com/mkuurstra-works/cookiecutter/cookiecutter-python) project template.
